package ru.itpark.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import ru.itpark.exception.*;

@ControllerAdvice
public class AppControllerAdvice extends ExceptionHandlerExceptionResolver {

    @ExceptionHandler(UnsupportedMediaTypeException.class)
    public String handlerUnsupportedMedia(RuntimeException e){
        return "errors/errorUnsupportedMedia";
    }

    @ExceptionHandler(NoSuchWorkerException.class)
    public String handlerNotSuchWorkerMedia(RuntimeException e){
        return "errors/errorNoSuchWorker";
    }

    @ExceptionHandler(ListOfWorkersEmptyException.class)
    public String handlerListOfWorkersEmpty(RuntimeException e){
        return "errors/errorListOfWorkersEmpty";
    }

    @ExceptionHandler({ExecutedWorkNotFoundException.class,
            FeedbackNotFoundException.class,
            ImageNotFoundException.class,
            MediaRemoveException.class,
            MediaUploadException.class,
            WorkerNotFoundException.class
    })
    public String handlerAllOtherExc(RuntimeException e){
        return "errors/error";
    }






}
