package ru.itpark.service;

import ru.itpark.dto.WorkerSaveDTO;
import ru.itpark.entity.FeedbackEntity;
import ru.itpark.entity.WorkerEntity;

import java.util.List;

public interface WorkerService {
    List<WorkerEntity> findAll();

    List<WorkerEntity> findByName(String name);


    WorkerEntity findById(int id);

    List<WorkerEntity> findByPriceInDescOrder();

    List<WorkerEntity> findByPriceInAscOrder();

    List<WorkerEntity> findByRatingInDescOrder();

    List<WorkerEntity> findByRatingInAscOrder();

    List<WorkerEntity> findByFeedbackInDescOrder();

    List<WorkerEntity> findByFeedbackInAscOrder();

    List<WorkerEntity> findByIsCarTrue();

    List<WorkerEntity> findByIsCarFalse();

    void deleteWorkerById(int id);

    void save(WorkerSaveDTO workerDto);

    void editWorkerById(int id, WorkerSaveDTO workerDto);
}
