package ru.itpark.service;

import ru.itpark.dto.ExecutedWorkSaveDTO;
import ru.itpark.entity.ExecutedWorkEntity;
import ru.itpark.entity.ImageEntity;
import ru.itpark.entity.WorkerEntity;

import java.util.List;

public interface ExecutedWorkService {


    WorkerEntity findWorkerByExecutedWorkId(int id);

    List<ImageEntity> findListOfImagesByExecutedWorkId(int id);

    String findNameOfWorkByExecutedWorkId(int id);

    void saveExecutedWorkByWorkerId(int id, ExecutedWorkSaveDTO executedWorkSaveDTO);

    ExecutedWorkEntity findById(int id);


    void deleteExecutedWorkById(int executedWorkId);

    void editExecutedWorkByTwoId(int workerId, int executedWorkId, ExecutedWorkSaveDTO executedWorkDto);

}
