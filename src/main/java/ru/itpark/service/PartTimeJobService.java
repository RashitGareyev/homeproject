package ru.itpark.service;

public interface PartTimeJobService {
    void initialData();

    void secondData();
}
