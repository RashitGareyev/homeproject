package ru.itpark.service;

import org.springframework.stereotype.Service;
import ru.itpark.entity.ExecutedWorkEntity;
import ru.itpark.entity.FeedbackEntity;
import ru.itpark.entity.ImageEntity;
import ru.itpark.entity.WorkerEntity;
import ru.itpark.repository.ExecutedWorkRepository;
import ru.itpark.repository.FeedbackRepository;
import ru.itpark.repository.ImageRepository;
import ru.itpark.repository.WorkerRepository;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class PartTimeJobServiceImpl implements PartTimeJobService {

    private final WorkerRepository workerRepository;
    private final FeedbackRepository feedbackRepository;
    private final ExecutedWorkRepository executedWorkRepository;
    private final ImageRepository imageRepository;

    public PartTimeJobServiceImpl(WorkerRepository workerRepository, FeedbackRepository feedbackRepository, ExecutedWorkRepository executedWorkRepository, ImageRepository imageRepository) {
        this.workerRepository = workerRepository;
        this.feedbackRepository = feedbackRepository;
        this.executedWorkRepository = executedWorkRepository;
        this.imageRepository = imageRepository;
    }

    @Override
    public void initialData() {

        //Нагиев начало блока

        ImageEntity imageWoodNagiev=new ImageEntity("wood1.jpg");
        ImageEntity imageBricksNagiev1=new ImageEntity("bricks1.jpg");
        ImageEntity imageBricksNagiev2=new ImageEntity("bricks2.jpg");


        ExecutedWorkEntity executedWorkWoodNagiev=new ExecutedWorkEntity(
                "Рубка дров",
                "Шесть березовых бревен длиной по 3 метра были распилены и наколоты на дрова",
                2000
        );
        ExecutedWorkEntity executedWorkBricksNagiev=new ExecutedWorkEntity(
                "Разбор кирпичного здания",
                "Старое кирпичное здание 5х6 м, 1,5 этажа. Толщина стен 1 кирпич, здание без внутренних перегородок",
                6000
        );


        FeedbackEntity feedbackWoodNagiev=new FeedbackEntity(
                "Работу сделал качественно и в срок",
                "Владимир",
                5
        );
        FeedbackEntity feedbackBricksNagiev=new FeedbackEntity(
                "Работу сделал, здание снес, но работал шумно. Сломал две кувалды",
                "Светлана",
                4
        );



        WorkerEntity workerNagiev=new WorkerEntity(
                0,
                "Вася",
                22,
                false,
                "8-917-5-864-123",
                "nagiev.jpg",
                1900
        );

        imageWoodNagiev.setExecutedWork(executedWorkWoodNagiev);
        imageBricksNagiev1.setExecutedWork(executedWorkBricksNagiev);
        imageBricksNagiev2.setExecutedWork(executedWorkBricksNagiev);
        imageRepository.saveAll(List.of(imageWoodNagiev, imageBricksNagiev1, imageBricksNagiev2));

        executedWorkWoodNagiev.setListOfImages(List.of(imageWoodNagiev));
        executedWorkWoodNagiev.setWorker(workerNagiev);
        executedWorkBricksNagiev.setListOfImages(List.of(imageBricksNagiev1, imageBricksNagiev2));
        executedWorkBricksNagiev.setWorker(workerNagiev);
        executedWorkRepository.saveAll(List.of(executedWorkWoodNagiev, executedWorkBricksNagiev));




        feedbackWoodNagiev.setWorker(workerNagiev);
        feedbackBricksNagiev.setWorker(workerNagiev);
        feedbackRepository.saveAll(List.of(feedbackWoodNagiev, feedbackBricksNagiev));

        workerNagiev.setListOfExecutedWork(List.of(executedWorkWoodNagiev, executedWorkBricksNagiev));
        workerNagiev.setListOfFeedback(List.of(feedbackWoodNagiev, feedbackBricksNagiev));
        workerRepository.save(workerNagiev);

        //Нагиев конец блока




        //Рейнольдс начало блока

        ImageEntity imageWoodReinolds1=new ImageEntity("wood2.jpg");
        ImageEntity imageWoodReinolds2=new ImageEntity("wood4.jpg");

        ImageEntity imageRelocationReinolds1=new ImageEntity("relocation1.jpg");
        ImageEntity imageRelocationReinolds2=new ImageEntity("relocation2.jpg");
        ImageEntity imageRelocationReinolds3=new ImageEntity("relocation3.jpg");



        ExecutedWorkEntity executedWorkWoodReinolds=new ExecutedWorkEntity(
                "Колка дров",
                "Десять березовых бревен длиной по 5 метров были распилены и наколоты на дрова",
                6000
        );
        ExecutedWorkEntity executedWorkRelocationReinolds=new ExecutedWorkEntity(
                "Переезд, погрузка / разрузка",
                "6 человек перевозили вещи семьи из 4-х человек из трехкомнатной квартиры в частный дом",
                2000
        );


        FeedbackEntity feedbackRelocationReinolds=new FeedbackEntity(
                "Помогали перевозить вещи. При переезде помяли дверцу холодильника",
                "Сергей",
                2
        );

        WorkerEntity workerReinolds=new WorkerEntity(
                0,
                "Петя",
                23,
                false,
                "8-917-3-584-136",
                "reinolds.jpg",
                2200
        );


        imageWoodReinolds1.setExecutedWork(executedWorkWoodReinolds);
        imageWoodReinolds2.setExecutedWork(executedWorkWoodReinolds);
        imageRelocationReinolds1.setExecutedWork(executedWorkRelocationReinolds);
        imageRelocationReinolds2.setExecutedWork(executedWorkRelocationReinolds);
        imageRelocationReinolds3.setExecutedWork(executedWorkRelocationReinolds);
        imageRepository.saveAll(List.of(imageWoodReinolds1, imageWoodReinolds2, imageRelocationReinolds1, imageRelocationReinolds2, imageRelocationReinolds3));


        executedWorkWoodReinolds.setListOfImages(List.of(imageWoodReinolds1, imageWoodReinolds2));
        executedWorkWoodReinolds.setWorker(workerReinolds);
        executedWorkRelocationReinolds.setListOfImages(List.of(imageRelocationReinolds1, imageRelocationReinolds2, imageRelocationReinolds3));
        executedWorkRelocationReinolds.setWorker(workerReinolds);
        executedWorkRepository.saveAll(List.of(executedWorkWoodReinolds, executedWorkRelocationReinolds));





        feedbackRelocationReinolds.setWorker(workerReinolds);
        feedbackRepository.saveAll(List.of(feedbackRelocationReinolds));

        workerReinolds.setListOfExecutedWork(List.of(executedWorkWoodReinolds, executedWorkRelocationReinolds));
        workerReinolds.setListOfFeedback(List.of(feedbackRelocationReinolds));
        workerRepository.save(workerReinolds);

        //Рейнольдс конец блока


        //Стетхэм начало блока

        ImageEntity imagePotatoStethem1=new ImageEntity("potato1.jpg");
        ImageEntity imageRelocationStethem1=new ImageEntity("relocation4.jpg");
        ImageEntity imageFoundationStethem1=new ImageEntity("foundation1.jpg");


        ExecutedWorkEntity executedWorkPotatoStethem=new ExecutedWorkEntity(
                "Уборка картофеля",
                "Нужно было убрать 10 соток картошки",
                3000
        );
        ExecutedWorkEntity executedWorkRelocationStethem=new ExecutedWorkEntity(
                "Переезд, погрузка / разрузка",
                "Нужно было перевезти человека в багажнике из одного города в другой",
                10000
        );
        ExecutedWorkEntity executedWorkFoundationStethem=new ExecutedWorkEntity(
                "Фундамент",
                "Сделал фундамент для кирпичного дома (круглогодичное проживание) 10х7 метров",
                35000
        );


        FeedbackEntity feedbackPotatoStethem=new FeedbackEntity(
                "Выкопал 10 соток картофеля, я только помогал ему собирать картофель и убирать все в мешки",
                "Марат",
                5
        );

        FeedbackEntity feedbackRelocationStethem=new FeedbackEntity(
                "Работу сделал, но не идеально",
                "Дон Карлеоне",
                4
        );



        FeedbackEntity feedbackFoundationStethem=new FeedbackEntity(
                "Фундамент для дома 10х7 метров сделал качественно. Я сам следил за работой.",
                "Иван",
                5
        );


        WorkerEntity workerStethem=new WorkerEntity(
                0,
                "Вова",
                27,
                true,
                "8-917-9-998-888",
                "stethem.jpg",
                2000
        );

        imagePotatoStethem1.setExecutedWork(executedWorkPotatoStethem);
        imageRelocationStethem1.setExecutedWork(executedWorkRelocationStethem);
        imageFoundationStethem1.setExecutedWork(executedWorkFoundationStethem);
        imageRepository.saveAll(List.of(imagePotatoStethem1, imageRelocationStethem1, imageFoundationStethem1));

        executedWorkPotatoStethem.setListOfImages(List.of(imagePotatoStethem1));
        executedWorkPotatoStethem.setWorker(workerStethem);
        executedWorkRelocationStethem.setListOfImages(List.of(imageRelocationStethem1));
        executedWorkRelocationStethem.setWorker(workerStethem);
        executedWorkFoundationStethem.setListOfImages(List.of(imageFoundationStethem1));
        executedWorkFoundationStethem.setWorker(workerStethem);
        executedWorkRepository.saveAll(List.of(executedWorkPotatoStethem, executedWorkRelocationStethem, executedWorkFoundationStethem));

        feedbackPotatoStethem.setWorker(workerStethem);
        feedbackFoundationStethem.setWorker(workerStethem);
        feedbackRelocationStethem.setWorker(workerStethem);
        feedbackRepository.saveAll(List.of(feedbackPotatoStethem, feedbackRelocationStethem, feedbackFoundationStethem));

        workerStethem.setListOfExecutedWork(List.of(executedWorkPotatoStethem, executedWorkRelocationStethem, executedWorkFoundationStethem));
        workerReinolds.setListOfFeedback(List.of(feedbackPotatoStethem, feedbackRelocationStethem, feedbackFoundationStethem));
        workerRepository.save(workerStethem);

        //Стетхэм конец блока



    }


    @Override
    public void secondData() {
        for (WorkerEntity workerEntity : workerRepository.findAll()) {
            if (workerEntity.isCar()) {
                workerEntity.setCarString("есть");
            } else {
                workerEntity.setCarString("нет");
            }

            int sum = 0;
            for (FeedbackEntity feedbackEntity : workerEntity.getListOfFeedback()) {
                sum += feedbackEntity.getRating();
            }
            float result = (float) sum / workerEntity.getListOfFeedback().size();
            float avrRatingTemp = 10 * result;
            int avrRatingInt = (int) avrRatingTemp;
            float avrRating = (float) avrRatingInt / 10;
            workerEntity.setAvrRatingInt(avrRatingInt);
            workerEntity.setAvrRating(avrRating);
            workerRepository.save(workerEntity);
        }
    }
}
