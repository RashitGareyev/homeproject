package ru.itpark.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import ru.itpark.dto.WorkerSaveDTO;
import ru.itpark.entity.ExecutedWorkEntity;
import ru.itpark.entity.ImageEntity;
import ru.itpark.entity.WorkerEntity;
import ru.itpark.exception.*;
import ru.itpark.repository.ExecutedWorkRepository;
import ru.itpark.repository.WorkerRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class WorkerServiceImpl implements WorkerService {
    private final WorkerRepository workerRepository;
    private final ExecutedWorkRepository executedWorkRepository;
    private final Path avatarMediaPath;
    private final Path imagesOfExecutedWorkMediaPath;
    private final PartTimeJobService partTimeJobService;


    public WorkerServiceImpl(
            WorkerRepository workerRepository,
            ExecutedWorkRepository executedWorkRepository,
            @Value("${app.media-path-avatars}") String avatarMediaPath,
            @Value("${app.media-path-images}") String imagesOfExecutedWorkMediaPath,
            PartTimeJobService partTimeJobService
    ) {
        this.workerRepository = workerRepository;
        this.executedWorkRepository = executedWorkRepository;
        this.avatarMediaPath = Paths.get(avatarMediaPath);
        this.imagesOfExecutedWorkMediaPath = Paths.get(imagesOfExecutedWorkMediaPath);
        this.partTimeJobService = partTimeJobService;
    }

    @Override
    public List<WorkerEntity> findAll() {
        partTimeJobService.secondData();


        return workerRepository.findAll();

    }

    @Override
    public WorkerEntity findById(int id) {
        return workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);
    }

    @Override
    public List<WorkerEntity> findByName(String name) {
        if (workerRepository.findByName("%" + name + "%").size() == 0) {
            throw new NoSuchWorkerException();
        }
        return workerRepository.findByName("%" + name + "%");
    }

    @Override
    public List<WorkerEntity> findByPriceInDescOrder() {
        List<WorkerEntity> workers = workerRepository.findAll();
        if (workers.size() == 0) {
            throw new ListOfWorkersEmptyException();
        }
        Collections.sort(workers, (o1, o2) -> -(o1.getPrice() - o2.getPrice()));
        return workers;
    }

    @Override
    public List<WorkerEntity> findByPriceInAscOrder() {
        List<WorkerEntity> workers = workerRepository.findAll();
        if (workers.size() == 0) {
            throw new ListOfWorkersEmptyException();
        }
        Collections.sort(workers, (o1, o2) -> (o1.getPrice() - o2.getPrice()));
        return workers;
    }

    @Override
    public List<WorkerEntity> findByRatingInDescOrder() {
        List<WorkerEntity> workers = workerRepository.findAll();
        if (workers.size() == 0) {
            throw new ListOfWorkersEmptyException();
        }
        Collections.sort(workers, (o1, o2) -> -(o1.getAvrRatingInt() - o2.getAvrRatingInt()));
        return workers;
    }

    @Override
    public List<WorkerEntity> findByRatingInAscOrder() {
        List<WorkerEntity> workers = workerRepository.findAll();
        if (workers.size() == 0) {
            throw new ListOfWorkersEmptyException();
        }
        Collections.sort(workers, (o1, o2) -> (o1.getAvrRatingInt() - o2.getAvrRatingInt()));
        return workers;
    }

    @Override
    public List<WorkerEntity> findByFeedbackInDescOrder() {
        List<WorkerEntity> workers = workerRepository.findAll();
        if (workers.size() == 0) {
            throw new ListOfWorkersEmptyException();
        }
        Collections.sort(workers, (o1, o2) -> -(o1.getListOfFeedback().size() - o2.getListOfFeedback().size()));
        return workers;
    }

    @Override
    public List<WorkerEntity> findByFeedbackInAscOrder() {
        List<WorkerEntity> workers = workerRepository.findAll();
        if (workers.size() == 0) {
            throw new ListOfWorkersEmptyException();
        }
        Collections.sort(workers, (o1, o2) -> (o1.getListOfFeedback().size() - o2.getListOfFeedback().size()));
        return workers;
    }


    @Override
    public List<WorkerEntity> findByIsCarTrue() {
        List<WorkerEntity> workers = workerRepository.findAll();
        if (workers.size() == 0) {
            throw new ListOfWorkersEmptyException();
        }
        List<WorkerEntity> result = new ArrayList<>();
        for (WorkerEntity worker : workers) {
            if (worker.isCar()) {
                result.add(worker);
            }
        }
        return result;
    }


    @Override
    public List<WorkerEntity> findByIsCarFalse() {
        List<WorkerEntity> workers = workerRepository.findAll();
        if (workers.size() == 0) {
            throw new ListOfWorkersEmptyException();
        }
        List<WorkerEntity> result = new ArrayList<>();
        for (WorkerEntity worker : workers) {
            if (!worker.isCar()) {
                result.add(worker);
            }
        }
        return result;
    }

    @Override
    public void deleteWorkerById(int id) {
        var workerEntity = workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);
        Path avatarTarget = avatarMediaPath.resolve(workerEntity.getAvatar());

        var listOfExecutedWork = executedWorkRepository.findListOfExecutedWorkByWorkerId(id);
        for (ExecutedWorkEntity executedWorkEntity : listOfExecutedWork) {
            List<ImageEntity> listOfImages = executedWorkEntity.getListOfImages();
            for (ImageEntity imageEntity : listOfImages) {
                Path imageTarget = imagesOfExecutedWorkMediaPath.resolve(imageEntity.getNameOfImage());

                try {
                    Files.deleteIfExists(imageTarget);
                } catch (IOException e) {
                    throw new MediaRemoveException(e);
                }

            }
        }

        try {
            Files.deleteIfExists(avatarTarget);
        } catch (IOException e) {
            throw new MediaRemoveException(e);
        }
        workerRepository.delete(workerEntity);
    }


    @Override
    public void save(WorkerSaveDTO workerDto) {
        var contentTypeAvatar = workerDto.getAvatar().getContentType();

        String name = UUID.randomUUID().toString();
        String ext;

        if (MediaType.IMAGE_JPEG_VALUE.equals(contentTypeAvatar)) {
            ext = ".jpg";
        } else if (MediaType.IMAGE_PNG_VALUE.equals(contentTypeAvatar)) {
            ext = ".png";
        } else {
            throw new UnsupportedMediaTypeException(contentTypeAvatar);
        }

        Path target = avatarMediaPath.resolve(name + ext);

        try {
            workerDto.getAvatar().transferTo(target.toFile());
        } catch (IOException e) {
            throw new MediaUploadException(e);
        }

        if (workerDto.getIsCarForDTO().equals("true")) {
            WorkerEntity workerEntity = new WorkerEntity(0, workerDto.getName(), workerDto.getAge(), true, workerDto.getPhone(), name + ext, workerDto.getPrice());
            workerRepository.save(workerEntity);
        } else {
            WorkerEntity workerEntity = new WorkerEntity(0, workerDto.getName(), workerDto.getAge(), false, workerDto.getPhone(), name + ext, workerDto.getPrice());
            workerRepository.save(workerEntity);
        }
    }



    @Override
    public void editWorkerById(int id, WorkerSaveDTO workerDto) {
        var initialWorkerEntity = workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);
        var contentTypeAvatar = workerDto.getAvatar().getContentType();
        String name;
        String ext;
        Path target;
        if (MediaType.IMAGE_JPEG_VALUE.equals(contentTypeAvatar)) {
            name = UUID.randomUUID().toString();
            ext = ".jpg";
            target = avatarMediaPath.resolve(name + ext);
            try {
                workerDto.getAvatar().transferTo(target.toFile());
            } catch (IOException e) {
                throw new MediaUploadException(e);
            }

            if (workerDto.getIsCarForDTO().equals("true")) {
                WorkerEntity workerEntity = new WorkerEntity(id, workerDto.getName(), workerDto.getAge(), true, workerDto.getPhone(), name + ext, workerDto.getPrice());
                workerRepository.save(workerEntity);
            } else {
                WorkerEntity workerEntity = new WorkerEntity(id, workerDto.getName(), workerDto.getAge(), false, workerDto.getPhone(), name + ext, workerDto.getPrice());
                workerRepository.save(workerEntity);
            }


        } else if (MediaType.IMAGE_PNG_VALUE.equals(contentTypeAvatar)) {
            name = UUID.randomUUID().toString();
            ext = ".png";
            target = avatarMediaPath.resolve(name + ext);
            try {
                workerDto.getAvatar().transferTo(target.toFile());
            } catch (IOException e) {
                throw new MediaUploadException(e);
            }
            if (workerDto.getIsCarForDTO().equals("true")) {
                WorkerEntity workerEntity = new WorkerEntity(id, workerDto.getName(), workerDto.getAge(), true, workerDto.getPhone(), name + ext, workerDto.getPrice());
                workerRepository.save(workerEntity);
            } else {
                WorkerEntity workerEntity = new WorkerEntity(id, workerDto.getName(), workerDto.getAge(), false, workerDto.getPhone(), name + ext, workerDto.getPrice());
                workerRepository.save(workerEntity);
            }
        } else if (workerDto.getAvatar().isEmpty()) {
            if (workerDto.getIsCarForDTO().equals("true")) {
                WorkerEntity workerEntity = new WorkerEntity(id, workerDto.getName(), workerDto.getAge(), true, workerDto.getPhone(), initialWorkerEntity.getAvatar(), workerDto.getPrice());
                workerRepository.save(workerEntity);
            } else {
                WorkerEntity workerEntity = new WorkerEntity(id, workerDto.getName(), workerDto.getAge(), false, workerDto.getPhone(), initialWorkerEntity.getAvatar(), workerDto.getPrice());
                workerRepository.save(workerEntity);
            }

        } else {
            throw new UnsupportedMediaTypeException(contentTypeAvatar);
        }
    }





}
