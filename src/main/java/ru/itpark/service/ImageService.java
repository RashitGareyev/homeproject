package ru.itpark.service;

import ru.itpark.dto.ImageSaveDTO;
import ru.itpark.entity.ImageEntity;

import java.util.List;

public interface ImageService {
    void saveImageByExecutedWorkId(int id, ImageSaveDTO imageSaveDTO);


    List<ImageEntity> findAll();

    ImageEntity findById(int imageId);

    void deleteImageByImageId(int imageId);
}
