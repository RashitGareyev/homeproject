package ru.itpark.service;

import ru.itpark.dto.FeedbackSaveDTO;
import ru.itpark.entity.FeedbackEntity;

public interface FeedbackService {

    void saveFeedbackByWorkerId (int id, FeedbackSaveDTO feedbackDto);


    void deleteFeedbackById(int id);

    FeedbackEntity findById(int id);

    void editFeedbackByTwoId(int workerId, int feedbackId, FeedbackSaveDTO feedbackDto);


}
