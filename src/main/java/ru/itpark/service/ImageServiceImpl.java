package ru.itpark.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import ru.itpark.dto.ImageSaveDTO;
import ru.itpark.entity.ExecutedWorkEntity;
import ru.itpark.entity.ImageEntity;
import ru.itpark.exception.*;
import ru.itpark.repository.ExecutedWorkRepository;
import ru.itpark.repository.ImageRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Service
public class ImageServiceImpl implements ImageService {
    private final ImageRepository imageRepository;
    private final ExecutedWorkRepository executedWorkRepository;
    private final Path imagesOfExecutedWorkMediaPath;

    public ImageServiceImpl(
            ImageRepository imageRepository,
            ExecutedWorkRepository executedWorkRepository,
            @Value("${app.media-path-images}") String imagesOfExecutedWorkMediaPath
    ) {
        this.imageRepository = imageRepository;
        this.executedWorkRepository = executedWorkRepository;
        this.imagesOfExecutedWorkMediaPath = Paths.get(imagesOfExecutedWorkMediaPath);
    }


    @Override
    public List<ImageEntity> findAll() {
        return imageRepository.findAll();
    }

    @Override
    public void saveImageByExecutedWorkId(int id, ImageSaveDTO imageSaveDTO) {
        var contentTypeImage = imageSaveDTO.getNameOfImage().getContentType();
        String name= UUID.randomUUID().toString();
        String ext;

        if (MediaType.IMAGE_JPEG_VALUE.equals(contentTypeImage)) {
            ext = ".jpg";
        } else if (MediaType.IMAGE_PNG_VALUE.equals(contentTypeImage)) {
            ext = ".png";
        } else {
            throw new UnsupportedMediaTypeException(contentTypeImage);
        }

        Path target=imagesOfExecutedWorkMediaPath.resolve(name+ext);


        try {
            imageSaveDTO.getNameOfImage().transferTo(target.toFile());
        } catch (IOException e) {
            throw new MediaUploadException(e);
        }



        ExecutedWorkEntity executedWorkEntity=executedWorkRepository.findById(id).orElseThrow(ExecutedWorkNotFoundException::new);
        ImageEntity imageEntity=new ImageEntity(name+ext);

        imageEntity.setExecutedWork(executedWorkEntity);
        imageRepository.save(imageEntity);

        executedWorkEntity.getListOfImages().add(imageEntity);
        executedWorkRepository.save(executedWorkEntity);
    }


    @Override
    public ImageEntity findById(int imageId) {
        return imageRepository.findById(imageId).orElseThrow(ImageNotFoundException::new);
    }

    @Override
    public void deleteImageByImageId(int imageId) {
        var imageEntity = imageRepository.findById(imageId).orElseThrow(ImageNotFoundException::new);
        Path imageTarget=imagesOfExecutedWorkMediaPath.resolve(imageEntity.getNameOfImage());
        try {
            Files.deleteIfExists(imageTarget);
        } catch (IOException e) {
            throw new MediaRemoveException(e);
        }

        imageRepository.delete(imageEntity);
    }


}
