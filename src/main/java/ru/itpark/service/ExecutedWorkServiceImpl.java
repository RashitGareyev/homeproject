package ru.itpark.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.itpark.dto.ExecutedWorkSaveDTO;
import ru.itpark.entity.ExecutedWorkEntity;
import ru.itpark.entity.ImageEntity;
import ru.itpark.entity.WorkerEntity;
import ru.itpark.exception.ExecutedWorkNotFoundException;
import ru.itpark.exception.MediaRemoveException;
import ru.itpark.exception.WorkerNotFoundException;
import ru.itpark.repository.ExecutedWorkRepository;
import ru.itpark.repository.WorkerRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class ExecutedWorkServiceImpl implements ExecutedWorkService{
    private final ExecutedWorkRepository executedWorkRepository;
    private final WorkerRepository workerRepository;
    private final Path imagesOfExecutedWorkMediaPath;

    public ExecutedWorkServiceImpl(
            ExecutedWorkRepository executedWorkRepository,
            WorkerRepository workerRepository,
            @Value("${app.media-path-images}") String imagesOfExecutedWorkMediaPath
            ) {
        this.executedWorkRepository = executedWorkRepository;
        this.workerRepository = workerRepository;
        this.imagesOfExecutedWorkMediaPath = Paths.get(imagesOfExecutedWorkMediaPath);
    }


    @Override
    public WorkerEntity findWorkerByExecutedWorkId(int id) {
        return executedWorkRepository.findWorkerByExecutedWorkId(id);
    }

    @Override
    public List<ImageEntity> findListOfImagesByExecutedWorkId(int id) {
        return executedWorkRepository.findListOfImagesByExecutedWorkId(id);
    }

    @Override
    public String findNameOfWorkByExecutedWorkId(int id) {
        return executedWorkRepository.findNameOfWorkByExecutedWorkId(id);
    }

    @Override
    public void saveExecutedWorkByWorkerId(int id, ExecutedWorkSaveDTO executedWorkSaveDTO) {
        WorkerEntity workerEntity= workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);
        ExecutedWorkEntity executedWorkEntity=new ExecutedWorkEntity(executedWorkSaveDTO.getNameOfWork(), executedWorkSaveDTO.getDescription(), executedWorkSaveDTO.getFullCost());

        executedWorkEntity.setWorker(workerEntity);
        executedWorkRepository.save(executedWorkEntity);
        workerEntity.getListOfExecutedWork().add(executedWorkEntity);
        workerRepository.save(workerEntity);

    }

    @Override
    public ExecutedWorkEntity findById(int id) {
        return executedWorkRepository.findById(id).orElseThrow(ExecutedWorkNotFoundException::new);
    }

    @Override
    public void deleteExecutedWorkById(int executedWorkId) {
        var executedWorkEntity=executedWorkRepository.findById(executedWorkId).orElseThrow(ExecutedWorkNotFoundException::new);
        var listOfImages=executedWorkRepository.findListOfImagesByExecutedWorkId(executedWorkId);

        for (ImageEntity imageEntity : listOfImages) {
            Path imageTarget=imagesOfExecutedWorkMediaPath.resolve(imageEntity.getNameOfImage());

            try {
                Files.deleteIfExists(imageTarget);
            } catch (IOException e) {
                throw new MediaRemoveException(e);
            }
        }

        executedWorkRepository.delete(executedWorkEntity);

    }


    @Override
    public void editExecutedWorkByTwoId(int workerId, int executedWorkId, ExecutedWorkSaveDTO executedWorkDto) {
        var workerEntity=workerRepository.findById(workerId).orElseThrow(WorkerNotFoundException::new);
        var executedWorkEntity=new ExecutedWorkEntity(executedWorkId, executedWorkDto.getNameOfWork(), executedWorkDto.getDescription(), executedWorkDto.getFullCost());
        executedWorkEntity.setWorker(workerEntity);
        executedWorkRepository.save(executedWorkEntity);
        workerRepository.save(workerEntity);
    }


}


