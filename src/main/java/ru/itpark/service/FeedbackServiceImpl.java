package ru.itpark.service;

import org.springframework.stereotype.Service;
import ru.itpark.dto.FeedbackSaveDTO;
import ru.itpark.entity.FeedbackEntity;
import ru.itpark.entity.WorkerEntity;
import ru.itpark.exception.FeedbackNotFoundException;
import ru.itpark.exception.WorkerNotFoundException;
import ru.itpark.repository.FeedbackRepository;
import ru.itpark.repository.WorkerRepository;

@Service
public class FeedbackServiceImpl implements FeedbackService {
    private final FeedbackRepository feedbackRepository;
    private final WorkerRepository workerRepository;

    public FeedbackServiceImpl(FeedbackRepository feedbackRepository, WorkerRepository workerRepository) {
        this.feedbackRepository = feedbackRepository;
        this.workerRepository = workerRepository;
    }


    @Override
    public void saveFeedbackByWorkerId (int id, FeedbackSaveDTO feedbackDto) {
        WorkerEntity workerEntity=workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);
        FeedbackEntity feedbackEntity=new FeedbackEntity(feedbackDto.getDescription(), feedbackDto.getEmployer(), feedbackDto.getRating());
        feedbackEntity.setWorker(workerEntity);
        feedbackRepository.save(feedbackEntity);
        workerEntity.getListOfFeedback().add(feedbackEntity);
        workerRepository.save(workerEntity);
   }

    @Override
    public void deleteFeedbackById(int id) {
        feedbackRepository.deleteById(id);
    }

    @Override
    public FeedbackEntity findById(int id) {
        return feedbackRepository.findById(id).orElseThrow(FeedbackNotFoundException::new);
    }

    @Override
    public void editFeedbackByTwoId(int workerId, int feedbackId, FeedbackSaveDTO feedbackDto) {
        var workerEntity=workerRepository.findById(workerId).orElseThrow(WorkerNotFoundException::new);
        var feedbackEntity=new FeedbackEntity(feedbackId, feedbackDto.getDescription(), feedbackDto.getEmployer(), feedbackDto.getRating());
        feedbackEntity.setWorker(workerEntity);
        feedbackRepository.save(feedbackEntity);
        workerRepository.save(workerEntity);
    }
}
