package ru.itpark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itpark.entity.AccountEntity;
import ru.itpark.repository.AccountRepository;
import ru.itpark.service.PartTimeJobServiceImpl;

import java.util.List;

@SpringBootApplication
public class PartTimeJobThApplication {

    public static void main(String[] args) {

        var context=SpringApplication.run(PartTimeJobThApplication.class, args);

        context
                .getBean(PartTimeJobServiceImpl.class).initialData();



        context
                .getBean(PartTimeJobServiceImpl.class).secondData();


        var encoder=context.getBean(PasswordEncoder.class);
        context
                .getBean(AccountRepository.class)
                .saveAll(List.of(
                        new AccountEntity(
                                0,
                                "vasya",
                                encoder.encode("vasya"),
                                List.of(new SimpleGrantedAuthority("ADMIN")),
                                true,
                                true,
                                true,
                                true
                        ),
                        new AccountEntity(
                                0,
                                "petya",
                                encoder.encode("petya"),
                                List.of(new SimpleGrantedAuthority("ADMIN")),
                                true,
                                true,
                                true,
                                true
                        )

                ));
    }
}
