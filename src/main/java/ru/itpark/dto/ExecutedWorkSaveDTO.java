package ru.itpark.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExecutedWorkSaveDTO {
    @NotNull
    private String nameOfWork;
    @NotNull
    private String description;
    @NotNull
    private int fullCost;


}
