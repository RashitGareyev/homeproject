package ru.itpark.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackSaveDTO {
    @NotNull
    @Size(min = 2, max = 50)
    private String employer;
    @NotNull
    @Size(min = 1, max = 800)
    private String description;
    @NotNull
    @DecimalMax("5")
    @DecimalMin("1")
    private int rating;

}
