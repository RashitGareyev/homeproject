package ru.itpark.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkerSaveDTO {
    @NotNull
    @Size(min=3, max=50)
    private String name;
    @NotNull
    private int age;
    @NotNull
    private boolean isCar;
    @NotNull
    private String isCarForDTO;
    @NotNull
    private String phone;
    @NotNull
    private MultipartFile avatar;
    @NotNull
    private int price;

}
