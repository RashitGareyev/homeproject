package ru.itpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itpark.service.ExecutedWorkService;
import ru.itpark.service.ImageService;

@Controller
@RequestMapping("/shabashka/executedWork/images")
public class ImagesController {
    private final ImageService imageService;
    private final ExecutedWorkService executedWorkService;

    public ImagesController(ImageService imageService, ExecutedWorkService executedWorkService) {
        this.imageService = imageService;
        this.executedWorkService=executedWorkService;
    }

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("images", imageService.findAll());
        return "images";
    }


    @GetMapping("/by-executed-work/{id}")
    public String getListOfImagesByExecutedWorkId(@PathVariable int id, Model model) {
        model.addAttribute("worker", executedWorkService.findWorkerByExecutedWorkId(id));
        model.addAttribute("listOfImagesOfOneExecutedWork", executedWorkService.findListOfImagesByExecutedWorkId(id));
        model.addAttribute("nameOfWork", executedWorkService.findNameOfWorkByExecutedWorkId(id));
        return "images";
    }


}
