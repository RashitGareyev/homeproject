package ru.itpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itpark.service.WorkerService;

@Controller
@RequestMapping("/shabashka/")
public class WorkerController {
    private final WorkerService workerService;

    public WorkerController(WorkerService workerService) {
        this.workerService = workerService;
    }

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("workers", workerService.findAll());
        return "workers";
    }



    @RequestMapping("/search")
    public String findByName(
            @RequestParam String name,
            Model model
    ){
        model.addAttribute("search",name);
        model.addAttribute("workers",workerService.findByName(name));
        return "workers";
    }


    @GetMapping("/sortByPriceInDescOrder")
    public String sortByPriceInDescOrder(Model model){
        model.addAttribute("workers", workerService.findByPriceInDescOrder());
        return "workers";
    }

    @GetMapping("/sortByPriceInAscOrder")
    public String sortByPriceInAscOrder(Model model){
        model.addAttribute("workers", workerService.findByPriceInAscOrder());
        return "workers";
    }

    @GetMapping("/sortByRatingInDescOrder")
    public String sortByRatingInDescOrder(Model model){
        model.addAttribute("workers", workerService.findByRatingInDescOrder());
        return "workers";
    }

    @GetMapping("/sortByRatingInAscOrder")
    public String sortByRatingInAscOrder(Model model){
        model.addAttribute("workers", workerService.findByRatingInAscOrder());
        return "workers";
    }

    @GetMapping("/sortByFeedbackInDescOrder")
    public String sortByFeedbackInDescOrder(Model model){
        model.addAttribute("workers", workerService.findByFeedbackInDescOrder());
        return "workers";
    }

    @GetMapping("/sortByFeedbackInAscOrder")
    public String sortByFeedbackInAscOrder(Model model){
        model.addAttribute("workers", workerService.findByFeedbackInAscOrder());
        return "workers";
    }

    @GetMapping("/sortByisCarTrue")
    public String sortByIsCarTrue(Model model){
        model.addAttribute("workers", workerService.findByIsCarTrue());
        return "workers";
    }

    @GetMapping("/sortByisCarFalse")
    public String sortByIsCarFalse(Model model){
        model.addAttribute("workers", workerService.findByIsCarFalse());
        return "workers";
    }






}
