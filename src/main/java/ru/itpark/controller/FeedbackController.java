package ru.itpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itpark.service.FeedbackService;
import ru.itpark.service.WorkerService;

@Controller
@RequestMapping("/shabashka/feedback")
public class FeedbackController {
    private final FeedbackService feedbackService;
    private final WorkerService workerService;

    public FeedbackController(FeedbackService feedbackService, WorkerService workerService) {
        this.feedbackService = feedbackService;
        this.workerService = workerService;
    }

    @GetMapping("/by-worker/{id}")
    public String getListOfFeedback(@PathVariable int id, Model model) {
        model.addAttribute("worker", workerService.findById(id));
        return "feedback";
    }


}
