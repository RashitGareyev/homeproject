package ru.itpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.itpark.dto.ExecutedWorkSaveDTO;
import ru.itpark.dto.FeedbackSaveDTO;
import ru.itpark.dto.ImageSaveDTO;
import ru.itpark.dto.WorkerSaveDTO;
import ru.itpark.service.ExecutedWorkService;
import ru.itpark.service.FeedbackService;
import ru.itpark.service.ImageService;
import ru.itpark.service.WorkerService;

import javax.validation.Valid;

@Controller
@RequestMapping("/shabashka/admin")
public class AdminController {
    private final WorkerService workerService;
    private final FeedbackService feedbackService;
    private final ExecutedWorkService executedWorkService;
    private final ImageService imageService;

    public AdminController(WorkerService workerService, FeedbackService feedbackService, ExecutedWorkService executedWorkService, ImageService imageService) {
        this.workerService = workerService;
        this.feedbackService = feedbackService;
        this.executedWorkService = executedWorkService;
        this.imageService = imageService;
    }

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("workers", workerService.findAll());
        return "admin/adminpanel";
    }

    @PostMapping("/by-worker/{id}/delete")
    public String deleteWorkerById(@PathVariable int id) {
        workerService.deleteWorkerById(id);
        return "redirect:/shabashka/admin";
    }

    @RequestMapping("/search")
    public String findByName(
            @RequestParam String name,
            Model model
    ) {
        model.addAttribute("search", name);
        model.addAttribute("workers", workerService.findByName(name));
        return "admin/adminpanel";

    }


    @GetMapping("/sortByPriceInDescOrder")
    public String sortByPriceInDescOrder(Model model){
        model.addAttribute("workers", workerService.findByPriceInDescOrder());
        return "admin/adminpanel";
    }

    @GetMapping("/sortByPriceInAscOrder")
    public String sortByPriceInAscOrder(Model model){
        model.addAttribute("workers", workerService.findByPriceInAscOrder());
        return "admin/adminpanel";
    }

    @GetMapping("/sortByRatingInDescOrder")
    public String sortByRatingInDescOrder(Model model){
        model.addAttribute("workers", workerService.findByRatingInDescOrder());
        return "admin/adminpanel";
    }

    @GetMapping("/sortByRatingInAscOrder")
    public String sortByRatingInAscOrder(Model model){
        model.addAttribute("workers", workerService.findByRatingInAscOrder());
        return "admin/adminpanel";
    }

    @GetMapping("/sortByFeedbackInDescOrder")
    public String sortByFeedbackInDescOrder(Model model){
        model.addAttribute("workers", workerService.findByFeedbackInDescOrder());
        return "admin/adminpanel";
    }

    @GetMapping("/sortByFeedbackInAscOrder")
    public String sortByFeedbackInAscOrder(Model model){
        model.addAttribute("workers", workerService.findByFeedbackInAscOrder());
        return "admin/adminpanel";
    }

    @GetMapping("/sortByisCarTrue")
    public String sortByIsCarTrue(Model model){
        model.addAttribute("workers", workerService.findByIsCarTrue());
        return "admin/adminpanel";
    }

    @GetMapping("/sortByisCarFalse")
    public String sortByIsCarFalse(Model model){
        model.addAttribute("workers", workerService.findByIsCarFalse());
        return "admin/adminpanel";
    }


    @GetMapping("/linkToSaveWorker")
    public String linkToSaveWorker(){
        return "admin/saveWorker";
    }



    @PostMapping("/saveWorker")
    public String save(@Valid @ModelAttribute WorkerSaveDTO workerDto) {
        workerService.save(workerDto);
        return "redirect:/shabashka/admin";
    }

    @GetMapping("/feedback/by-worker/{id}")
    public String getListOfFeedback(@PathVariable int id, Model model) {
        model.addAttribute("worker", workerService.findById(id));
//        model.addAttribute("feedback",feedbackService.findById(id));
        return "admin/feedback";
    }

    @GetMapping("/executedWork/by-worker/{id}")
    public String getListOfExecutedWork(@PathVariable int id, Model model) {
        model.addAttribute("worker", workerService.findById(id));
        return "admin/executedWork";
    }

    @GetMapping("/executedWork/images/by-executed-work/{id}")
    public String getListOfImagesByExecutedWorkId(@PathVariable int id, Model model) {
        model.addAttribute("worker", executedWorkService.findWorkerByExecutedWorkId(id));
        model.addAttribute("listOfImagesOfOneExecutedWork", executedWorkService.findListOfImagesByExecutedWorkId(id));
        model.addAttribute("nameOfWork", executedWorkService.findNameOfWorkByExecutedWorkId(id));
        model.addAttribute("executedWork", executedWorkService.findById(id));
        return "admin/images";
    }




    @GetMapping("/saveFeedback/by-worker/{id}")
    public String getWorkerById(@PathVariable int id, Model model) {
        model.addAttribute("worker", workerService.findById(id));
        return "admin/saveFeedback";
    }


    @PostMapping("/saveFeedback/by-worker/{id}")
    public String saveFeedbackByWorkerId(@PathVariable int id, @Valid @ModelAttribute FeedbackSaveDTO feedbackDto, Model model) {
        feedbackService.saveFeedbackByWorkerId(id, feedbackDto);
        model.addAttribute("worker", workerService.findById(id));
        return "redirect:/shabashka/admin/feedback/by-worker/{id}";
    }

    @GetMapping("/saveExecutedWork/by-worker/{id}")
    public String linkExecutedWorkByWorkerId(@PathVariable int id, Model model) {
        model.addAttribute("worker", workerService.findById(id));
        return "admin/saveExecutedWork";
    }


    @PostMapping("/saveExecutedWork/by-worker/{id}")
    public String saveExecutedWorkByWorkerId(@PathVariable int id, @Valid @ModelAttribute ExecutedWorkSaveDTO executedWorkSaveDTO, Model model) {
        executedWorkService.saveExecutedWorkByWorkerId(id, executedWorkSaveDTO);
        model.addAttribute("worker", workerService.findById(id));
        return "redirect:/shabashka/admin/executedWork/by-worker/{id}";
    }






    @GetMapping("/saveImage/by-executed-work/{id}")
    public String linkImagesByExecutedWorkId(@PathVariable int id, Model model) {
        model.addAttribute("executedWork", executedWorkService.findById(id));
        return "admin/saveImage";
    }





    @PostMapping("/saveImage/by-executed-work/{id}")
    public String saveImageByExecutedWorkId(@PathVariable int id, @Valid @ModelAttribute ImageSaveDTO imageSaveDTO, Model model) {
        model.addAttribute("executedWork", executedWorkService.findById(id));
        model.addAttribute("worker",executedWorkService.findWorkerByExecutedWorkId(id));
        model.addAttribute("listOfImagesOfOneExecutedWork", executedWorkService.findListOfImagesByExecutedWorkId(id));
        imageService.saveImageByExecutedWorkId(id, imageSaveDTO);
        return "redirect:/shabashka/admin/executedWork/images/by-executed-work/{id}";
    }

    @PostMapping("/feedback/by-worker/{workerId}/by-feedback/{feedbackId}/delete")
    public String deleteFeedbackById(@PathVariable int workerId, @PathVariable int feedbackId) {
        feedbackService.deleteFeedbackById(feedbackId);
        return "redirect:/shabashka/admin/feedback/by-worker/{workerId}";
    }


    @GetMapping("/feedback/by-worker/{workerId}/by-feedback/{feedbackId}/delete")
    public String get(@PathVariable int workerId, @PathVariable int feedbackId, Model model) {
        model.addAttribute("worker", workerService.findById(workerId));
        model.addAttribute("feedback",feedbackService.findById(feedbackId));
        return "admin/feedback";
    }

    @PostMapping("/executedWork/by-worker/{workerId}/by-executed-work/{executedWorkId}/delete")
    public String deleteExecutedWorkById(@PathVariable int workerId, @PathVariable int executedWorkId) {
        executedWorkService.deleteExecutedWorkById(executedWorkId);
        return "redirect:/shabashka/admin/executedWork/by-worker/{workerId}";
    }


    @GetMapping("/executedWork/by-worker/{workerId}/by-executed-work/{executedWorkId}/delete")
    public String getForExecutedWork(@PathVariable int workerId, @PathVariable int executedWorkId, Model model) {
        model.addAttribute("worker", workerService.findById(workerId));
        model.addAttribute("executedWork",executedWorkService.findById(executedWorkId));
        return "admin/executedWork";
    }

    @PostMapping("/executedWork/images/by-executed-work/{executedWorkId}/by-image/{imageId}/delete")
    public String deleteImageById(@PathVariable int imageId, @PathVariable int executedWorkId) {
        imageService.deleteImageByImageId(imageId);
        return "redirect:/shabashka/admin/executedWork/images/by-executed-work/{executedWorkId}";
    }


    @GetMapping("/executedWork/images/by-executed-work/{executedWorkId}/by-image/{imageId}/delete")
    public String getForImage(@PathVariable int imageId, @PathVariable int executedWorkId, Model model) {
        model.addAttribute("executedWork",executedWorkService.findById(executedWorkId));
        return "admin/images";
    }

    @PostMapping("/editWorker/{id}")
    public String editWorker(@PathVariable int id, @Valid @ModelAttribute WorkerSaveDTO workerDto){
        workerService.editWorkerById(id, workerDto);
        return "redirect:/shabashka/admin";
    }

    @GetMapping("/editWorker/{id}")
    public String getEditWorker(@PathVariable int id, Model model){
        model.addAttribute("worker", workerService.findById(id));
        return "admin/editWorker";
    }

    @PostMapping("/editFeedback/by-worker/{workerId}/by-feedback/{feedbackId}")
    public String editFeedback(@PathVariable int workerId, @PathVariable int feedbackId, @Valid @ModelAttribute FeedbackSaveDTO feedbackDto){
        feedbackService.editFeedbackByTwoId(workerId, feedbackId, feedbackDto);
        return "redirect:/shabashka/admin/feedback/by-worker/{workerId}";
    }

    @GetMapping("/editFeedback/by-worker/{workerId}/by-feedback/{feedbackId}")
    public String getEditFeedback(@PathVariable int workerId, @PathVariable int feedbackId, Model model){
        model.addAttribute("worker", workerService.findById(workerId));
        model.addAttribute("feedback",feedbackService.findById(feedbackId));
        return "admin/editFeedback";
    }

    @PostMapping("/editExecutedWork/by-worker/{workerId}/by-executed-work/{executedWorkId}")
    public String editExecutedWork(@PathVariable int workerId, @PathVariable int executedWorkId, @Valid @ModelAttribute ExecutedWorkSaveDTO executedWorkDto){
        executedWorkService.editExecutedWorkByTwoId(workerId, executedWorkId, executedWorkDto);
        return "redirect:/shabashka/admin/executedWork/by-worker/{workerId}";
    }

    @GetMapping("/editExecutedWork/by-worker/{workerId}/by-executed-work/{executedWorkId}")
    public String getEditExecutedWork(@PathVariable int workerId, @PathVariable int executedWorkId, Model model){
        model.addAttribute("worker", workerService.findById(workerId));
        model.addAttribute("executedWork",executedWorkService.findById(executedWorkId));
        return "admin/editExecutedWork";
    }







}
