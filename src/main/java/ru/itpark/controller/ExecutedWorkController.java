package ru.itpark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itpark.service.ExecutedWorkService;
import ru.itpark.service.ImageService;
import ru.itpark.service.WorkerService;

@Controller
@RequestMapping("/shabashka/executedWork")
public class ExecutedWorkController {
    private final ExecutedWorkService executedWorkService;
    private final ImageService imageService;
    private final WorkerService workerService;


    public ExecutedWorkController(ExecutedWorkService executedWorkService, ImageService imageService, WorkerService workerService) {
        this.executedWorkService = executedWorkService;
        this.imageService = imageService;
        this.workerService=workerService;
    }

    @GetMapping("/by-worker/{id}")
    public String getListOfExecutedWork(@PathVariable int id, Model model) {
        model.addAttribute("worker", workerService.findById(id));
        return "executedWork";
    }



}
