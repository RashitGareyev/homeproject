package ru.itpark.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "executedWork")

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExecutedWorkEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(nullable = false)
    private String nameOfWork;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private int fullCost;
    @ManyToOne
    private WorkerEntity worker;
    @OneToMany(mappedBy = "executedWork", cascade = CascadeType.ALL)
    List<ImageEntity> listOfImages=new ArrayList<>();

    public ExecutedWorkEntity(int id, String nameOfWork, String description, int fullCost) {
        this.id=id;
        this.nameOfWork = nameOfWork;
        this.description = description;
        this.fullCost = fullCost;
    }

    public ExecutedWorkEntity(String nameOfWork, String description, int fullCost) {
        this.nameOfWork = nameOfWork;
        this.description = description;
        this.fullCost = fullCost;
    }
}
