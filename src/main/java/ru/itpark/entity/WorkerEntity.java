package ru.itpark.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "worker")

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WorkerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private int age;
    @Column(nullable = false)
    private boolean isCar;
    @Column(nullable = true)
    private String carString;
    @Column(nullable = true)
    private float avrRating;
    @Column(nullable = true)
    private int avrRatingInt;
    @Column(nullable = false)
    private String phone;
    @Column(nullable = false)
    private String avatar;
    @Column(nullable = false)
    private int price;

    @OneToMany(mappedBy = "worker", cascade = CascadeType.ALL)
    List<FeedbackEntity> listOfFeedback=new ArrayList<>();
    @OneToMany(mappedBy = "worker", cascade = CascadeType.ALL)
    List<ExecutedWorkEntity> listOfExecutedWork=new ArrayList<>();

    public WorkerEntity(int id, String name, int age, boolean isCar, String phone, String avatar, int price) {
        this.id=id;
        this.name = name;
        this.age = age;
        this.isCar = isCar;
        this.phone = phone;
        this.avatar = avatar;
        this.price = price;
    }


}
