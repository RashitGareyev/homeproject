package ru.itpark.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@Table(name = "imagesOfExecutedWork")

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ImageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(nullable = false)
    private String nameOfImage;
    @ManyToOne
    private ExecutedWorkEntity executedWork;

    public ImageEntity(String nameOfImage) {

        this.nameOfImage = nameOfImage;
    }
}
