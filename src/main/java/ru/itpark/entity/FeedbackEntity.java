package ru.itpark.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
@Entity
@Table(name = "feedback")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class FeedbackEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private String employer;
    @Column(nullable = false)
    private int rating;
    @ManyToOne
    private WorkerEntity worker;

    public FeedbackEntity(String description, String employer, int rating) {
        this.description = description;
        this.employer = employer;
        this.rating = rating;
    }

    public FeedbackEntity(int id, String description, String employer, int rating) {
        this.id = id;
        this.description = description;
        this.employer = employer;
        this.rating = rating;
    }

}
