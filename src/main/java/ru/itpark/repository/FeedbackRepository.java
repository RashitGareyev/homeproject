package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itpark.entity.FeedbackEntity;
import ru.itpark.entity.WorkerEntity;

import java.util.List;

public interface FeedbackRepository extends JpaRepository<FeedbackEntity, Integer> {

}
