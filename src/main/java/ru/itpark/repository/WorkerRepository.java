package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itpark.entity.FeedbackEntity;
import ru.itpark.entity.WorkerEntity;

import java.util.List;

public interface WorkerRepository extends JpaRepository<WorkerEntity, Integer> {

    @Query("SELECT e FROM WorkerEntity e WHERE LOWER(e.name) LIKE LOWER(:name)")
    List<WorkerEntity> findByName(@Param("name") String name);

    @Query("SELECT e.listOfFeedback FROM WorkerEntity e WHERE LOWER(e.id) LIKE LOWER(:workerId)")
    List<FeedbackEntity> findListOfFeedbackByWorkerId(@Param("workerId") int id);
}
