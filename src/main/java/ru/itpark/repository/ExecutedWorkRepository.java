package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itpark.entity.ExecutedWorkEntity;
import ru.itpark.entity.ImageEntity;
import ru.itpark.entity.WorkerEntity;

import java.util.List;

public interface ExecutedWorkRepository extends JpaRepository<ExecutedWorkEntity, Integer> {

    @Query("SELECT e FROM ExecutedWorkEntity e WHERE e.worker.id=:workerId")
    List<ExecutedWorkEntity> findListOfExecutedWorkByWorkerId(@Param("workerId") int id);

    @Query("SELECT e.worker FROM ExecutedWorkEntity e WHERE e.id=:executedWorkId")
    WorkerEntity findWorkerByExecutedWorkId(@Param("executedWorkId") int id);

    @Query("SELECT e.listOfImages FROM ExecutedWorkEntity e WHERE e.id=:executedWorkId")
    List<ImageEntity> findListOfImagesByExecutedWorkId(@Param("executedWorkId") int id);

    @Query("SELECT e.nameOfWork FROM ExecutedWorkEntity e WHERE e.id=:executedWorkId")
    String findNameOfWorkByExecutedWorkId(@Param("executedWorkId") int id);
}
