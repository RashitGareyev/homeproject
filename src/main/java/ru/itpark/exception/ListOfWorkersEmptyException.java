package ru.itpark.exception;

public class ListOfWorkersEmptyException extends RuntimeException {
    public ListOfWorkersEmptyException() {
    }

    public ListOfWorkersEmptyException(String message) {
        super(message);
    }

    public ListOfWorkersEmptyException(String message, Throwable cause) {
        super(message, cause);
    }

    public ListOfWorkersEmptyException(Throwable cause) {
        super(cause);
    }

    public ListOfWorkersEmptyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
