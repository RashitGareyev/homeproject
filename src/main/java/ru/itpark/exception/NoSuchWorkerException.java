package ru.itpark.exception;

public class NoSuchWorkerException extends RuntimeException {
    public NoSuchWorkerException() {
    }

    public NoSuchWorkerException(String message) {
        super(message);
    }

    public NoSuchWorkerException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchWorkerException(Throwable cause) {
        super(cause);
    }

    public NoSuchWorkerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
