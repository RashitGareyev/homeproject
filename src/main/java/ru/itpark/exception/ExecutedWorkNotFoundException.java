package ru.itpark.exception;

public class ExecutedWorkNotFoundException extends RuntimeException{
    public ExecutedWorkNotFoundException() {
    }

    public ExecutedWorkNotFoundException(String message) {
        super(message);
    }

    public ExecutedWorkNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExecutedWorkNotFoundException(Throwable cause) {
        super(cause);
    }

    public ExecutedWorkNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
